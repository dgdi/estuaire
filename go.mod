module gitlab.com/dgdi/estuaire

go 1.13

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/monchemin/C-19-API v0.0.0-20200509202517-b6117244d2c5
)
