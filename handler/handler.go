package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/monchemin/C-19-API/connector/pgsql"
)

type Handler interface {
	CreateCenter(c *gin.Context)
	CreateService(c *gin.Context)
	CreateUser(c *gin.Context)

	CreateCustomer(c *gin.Context)
	CreateAppointment(c *gin.Context)
}
type handler struct {

}

func Setup(router *gin.Engine, pg *pgsql.DB) *gin.Engine {
	handler := handler{
	}
	patientRouter := router.Group("/center")
	{
		patientRouter.POST("/add", handler.CreateCenter)

	}

	routerPosition := router.Group("/service")
	{
		routerPosition.POST("/add", handler.CreateService)

	}

	routerAdmin := router.Group("/admin")
	{
		routerAdmin.POST("/login", handler.Login)
		routerAdmin.POST("/user", handler.CreateUser)

	}
	return router
}
