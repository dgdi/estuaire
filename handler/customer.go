package handler

import (
	"log"
	"net/http"

	"github.com/monchemin/C-19-API/position/model"

	"github.com/gin-gonic/gin"
)

func (h *handler) CreateCustomer(c *gin.Context) {
	var request model.CountryRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

}


